from tkinter import *


def initImg():
	tabText = ["black", "blue", "green", "red", "orange", "pink", "yellow", "purple", "grey", "white", "right", "top", "bot", "left", "pen", "change", "go", "undo","erase", "robit", "white2"]
	tabPhoto = {}
	for text in tabText : 
		tabPhoto[text] = PhotoImage(file = "./gifimage/" + text + ".gif")
	return tabPhoto


class Main:
	def __init__(self):
		fenetre = Tk()
		self.tabPhoto = initImg()
		self.fenetre = fenetre
		self.first_window = Frame(self.fenetre)
		self.second_window = Frame(self.fenetre)

		self.fenetre = fenetre
		self.fenetre.attributes('-fullscreen', True) 
	
		self.fenetre.title("Apprend à Rob à dessiner!") #Learn Rob to draw !
		self.create_button_color()
		self.create_grid()
		self.create_button_direction()
		self.create_stock_action()

		self.first_window.pack()
		self.second_window.pack()
		fenetre.mainloop()

	def create_button_color(self):
		self.window_left = Frame(self.first_window)

		self.bouton_black = Button(self.window_left, command=lambda :self.add_action("black"),image=self.tabPhoto["black"], width=50, height=50)
		self.bouton_blue = Button(self.window_left, command=lambda: self.add_action("blue"),image=self.tabPhoto["blue"],width=50, height=50)
		self.bouton_green = Button(self.window_left, command=lambda: self.add_action("green"),image=self.tabPhoto["green"],width=50, height=50)
		self.bouton_red = Button(self.window_left, command=lambda: self.add_action("red"),image=self.tabPhoto["red"],width=50, height=50)
		self.bouton_orange = Button(self.window_left, command=lambda: self.add_action("orange"),image=self.tabPhoto["orange"],width=50, height=50)
		self.bouton_pink = Button(self.window_left, command=lambda: self.add_action("pink"),image=self.tabPhoto["pink"],width=50, height=50)
		self.bouton_yellow = Button(self.window_left, command=lambda: self.add_action("yellow"),image=self.tabPhoto["yellow"],width=50, height=50)
		self.bouton_purple = Button(self.window_left, command=lambda: self.add_action("purple"),image=self.tabPhoto["purple"],width=50, height=50)
		self.bouton_grey = Button(self.window_left, command=lambda: self.add_action("grey"),image=self.tabPhoto["grey"],width=50, height=50)
		self.bouton_white = Button(self.window_left, command=lambda: self.add_action("white2"),image=self.tabPhoto["white"],width=50, height=50)
		
		self.window_left.pack(side="left")
		self.bouton_black.pack()
		self.bouton_blue.pack()
		self.bouton_green.pack()
		self.bouton_red.pack()
		self.bouton_orange.pack()
		self.bouton_pink.pack()
		self.bouton_yellow.pack()
		self.bouton_purple.pack()
		self.bouton_grey.pack()
		self.bouton_white.pack()

	def create_button_direction(self):
		self.window_right = Frame(self.first_window)
		self.window_right.pack(side="left")
		self.bouton_right = Button(self.window_right, command=lambda: self.add_action("right"), image=self.tabPhoto["right"], width=50, height=50)
		self.bouton_top = Button(self.window_right, command=lambda: self.add_action("top"),image=self.tabPhoto["top"], width=50, height=50)
		self.bouton_bot = Button(self.window_right, command=lambda: self.add_action("bot"),image=self.tabPhoto["bot"], width=50, height=50)
		self.bouton_left = Button(self.window_right, command=lambda: self.add_action("left"),image=self.tabPhoto["left"], width=50, height=50)
		self.bouton_pen = Button(self.window_right, command=lambda: self.add_action("pen"),image=self.tabPhoto["pen"], width=50, height=50)
		self.bouton_change = Button(self.window_right, command=lambda: self.add_action("change"),image=self.tabPhoto["change"], width=50, height=50)
		self.bouton_undo = Button(self.window_right, command=lambda: self.undo(),image=self.tabPhoto["undo"], width=50, height=50)
		self.bouton_reset = Button(self.window_right, command=lambda: self.reset(),image=self.tabPhoto["erase"], width=50, height=50)
		self.bouton_go = Button(self.window_right, command=lambda: self.add_action("go"),image=self.tabPhoto["go"], width=50, height=50)

		self.bouton_right.pack()
		self.bouton_top.pack()
		self.bouton_bot.pack()
		self.bouton_left.pack()
		self.bouton_pen.pack()
		self.bouton_change.pack()
		self.bouton_undo.pack()
		self.bouton_reset.pack()
		self.bouton_go.pack()

	def create_grid(self):
		self.window_center = Frame(self.first_window)
		self.window_center.grid(row=6, column=6)
		self.window_center.pack(side="left")
		self.tab_button = []
		for row in range(6):
			self.tab_button.append([])
			for column in range(6):
				button = Button(self.window_center, text=" ", padx=0, pady=0, image=self.tabPhoto["white2"], width=80, height=80)
				self.tab_button[row].append(button)
				button.grid(row=row, column=column)
		self.tab_button[0][0]["image"] = self.tabPhoto["robit"]

	def create_stock_action(self):
		self.window_bot = Frame(self.second_window)
		self.tab_frame = [self.window_bot]
		self.text_action = []
		self.button_action = []
		self.function_action= []
		self.window_bot.pack()
		self.nb_action = 0

	def add_action(self, text):
		if text == "go" :
			self.compute_position(self.text_action)
		else : 
			if self.nb_action >= 20 :
				self.nb_action = 0
				self.window_bot = Frame(self.second_window)
				self.tab_frame.append(self.window_bot)
				self.window_bot.pack()
			button = Button(self.window_bot, image=self.tabPhoto[text], width=50, height=50)
			self.text_action.append(text)
			self.button_action.append(button)
			button.grid(row=0, column=len(self.text_action)-1)
			self.nb_action += 1
	
	def compute_position(self, text_action):
		row = 0
		column = 0 
		color = "white"
		for text in text_action :
			if text == "right":
				column += 1
			elif text == "left":
				column -= 1
			elif text == "top":
				row -= 1
			elif text == "bot":
				row += 1
			elif text == "bot":
				row += 1
			elif text == "pen":
				self.tab_button[row][column]["image"] = self.tabPhoto[color]
			else :
				color = text 
    	
	def undo(self):
		for tab in self.tab_button :
			for button in tab :
				button["image"] = self.tabPhoto["white2"]
		
		self.tab_button[0][0]["image"] = self.tabPhoto["robit"]
		self.text_action.pop()
		if len(self.window_bot.grid_slaves()) == 0 :
			self.window_bot.destroy()
			self.tab_frame.pop()
			self.window_bot = self.tab_frame[-1]
		test = self.window_bot.grid_slaves()
		test[0].destroy()

	def reset(self):
		for frame in self.tab_frame :
			buttons = frame.grid_slaves()
			for button in buttons:
				button.destroy()
			frame.destroy()
		self.nb_action = 0
		self.window_bot = Frame(self.second_window)
		self.tab_frame = [self.window_bot]
		self.window_bot.pack()
		self.text_action = []
		for tab in self.tab_button :
			for button in tab :
				button["image"] = self.tabPhoto["white2"]
		self.tab_button[0][0]["image"] = self.tabPhoto["robit"]

Main()
